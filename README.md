## How to contribute

```
1. git remote add origin https://xditaba@bitbucket.org/xditaba/reactjs-webpack.git
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
```
